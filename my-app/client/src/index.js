import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Greeting from './Greeting';
//import * as serviceWorker from './serviceWorker';
 
ReactDOM.render(<Greeting />, document.getElementById('root'));
 

//serviceWorker.unregister();